package com.example.projetmobile_lucasdamiens;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projetmobile_lucasdamiens.webservice.JSONResponseHandlerCatalog;
import com.example.projetmobile_lucasdamiens.webservice.JSONResponseHandlerObjet;
import com.example.projetmobile_lucasdamiens.webservice.WebServiceUrl;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ObjetActivity extends AppCompatActivity {

    private TextView nameObjet;
    private EditText marque, year, categories, technicalDetails, description, working;
    private ListView images;
    private ProgressDialog progressDialog;
    private Objet objet;
    private String id;
    private CustomListViewAdapterPictures adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_objet);

        id = getIntent().getStringExtra("Id");

        MyAsyncTasks asyncTask = new MyAsyncTasks();
        asyncTask.execute();

    }

    public class MyAsyncTasks extends AsyncTask<String, String, Objet> {

        private String picture = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ObjetActivity.this);
            progressDialog.setMessage("Mise à jours en cours...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Objet doInBackground(String... params) {
            Objet item = new Objet(id);

            try {
                URL url;
                JSONResponseHandlerObjet JSONCatalog = new JSONResponseHandlerObjet(item);
                HttpURLConnection urlConnection = null;

                try {
                    url = WebServiceUrl.buildSearchItem(item.getId());
                    urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream in = urlConnection.getInputStream();

                    JSONCatalog.readJsonStream(in);

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                try {

                    for (int j = 0; j < item.getPictures().size(); j++) {
                        url = WebServiceUrl.buildSearchImages(item.getId(), item.getPictures().get(j));

                        InputStream is = (InputStream) new URL(url.toString()).getContent();

                        Drawable draw = Drawable.createFromStream(is, picture);

                        item.getImages().add(draw);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return item;
        }

        @Override
        protected void onPostExecute(Objet item)
        {
            progressDialog.dismiss();
            objet = item;

            updateObjet();
        }

    }

    public void updateObjet(){
        nameObjet = (TextView) findViewById(R.id.nameObjet);
        marque = (EditText) findViewById(R.id.editMarque);
        categories = (EditText) findViewById(R.id.editCategorie);
        year = (EditText) findViewById(R.id.editYear);
        technicalDetails = (EditText) findViewById(R.id.editDetails);
        description = (EditText) findViewById(R.id.editDescription);
        working = (EditText) findViewById(R.id.editEtat);

        images = (ListView) findViewById(R.id.images);

        nameObjet.setText(objet.getName());
        marque.setText(objet.getBrand());
        year.setText(objet.getYearAndTimeFrame());
        categories.setText(objet.getAllCategories());
        description.setText(objet.getDescription());
        technicalDetails.setText(objet.getAllDetails());
        working.setText(objet.getWorking());


        adapter = new CustomListViewAdapterPictures(this, objet);

        images.setAdapter(adapter);

    }

}
