package com.example.projetmobile_lucasdamiens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class CustomListViewAdapterPictures extends BaseAdapter {
    private Objet item;
    private LayoutInflater layoutInflater;
    private Context context;

    public CustomListViewAdapterPictures(Context context, Objet item){
        this.item=item;
        layoutInflater = LayoutInflater.from(context);
        this.context=context;
    }

    @Override
    public int getCount() { return item.getImages().size(); }

    @Override
    public Object getItem(int position) { return item.getPictures().get(position); }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        CustomListViewAdapterPictures.ViewHolder holder;

        if(convertView == null) {
            convertView = layoutInflater.inflate(R.layout.pictures, null);
            holder = new CustomListViewAdapterPictures.ViewHolder();

            holder.image = (ImageView) convertView.findViewById(R.id.picture);
            holder.desc = (TextView) convertView.findViewById(R.id.desc);


            convertView.setTag(holder);
        } else {
            holder = (CustomListViewAdapterPictures.ViewHolder) convertView.getTag();
        }

        holder.image.setImageDrawable(item.getImages().get(position));
        holder.desc.setText(item.getPicturesDescription().get(position));

        return convertView;
    }

    static class ViewHolder {
        ImageView image;
        TextView desc;
    }
}
