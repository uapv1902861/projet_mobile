package com.example.projetmobile_lucasdamiens.webservice;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class JSONResponseHandlerCategories {

    private ArrayList<String> categories;

    public JSONResponseHandlerCategories(ArrayList<String> categorie) { this.categories=categorie; }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try{
            readCategories(reader);
        } finally {
            reader.close();
        }
    }

    public void readCategories(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext())
        {
            String name = reader.nextString();
            categories.add(name);
        }
        reader.endArray();
    }

}
