package com.example.projetmobile_lucasdamiens.webservice;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    private static final String SEARCH_CATALOG = "catalog";

    public static URL buildSearchCatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String SEARCH_IDS = "ids";

    public static URL buildSearchIds() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_IDS);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String SEARCH_ITEM = "items";
    private static final String SEARCH_THUMBNAIL = "thumbnail";

    public static URL buildSearchThumbnail(String id) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEM);
        builder.appendPath(id);
        builder.appendPath(SEARCH_THUMBNAIL);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String SEARCH_IMAGE = "images";

    public static URL buildSearchImages(String idItem, String idImage) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEM);
        builder.appendPath(idItem);
        builder.appendPath(SEARCH_IMAGE);
        builder.appendPath(idImage);
        URL url = new URL(builder.build().toString());
        return url;
    }

    public static URL buildSearchItem(String idItem) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEM);
        builder.appendPath(idItem);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String SEARCH_CATEGORIE = "categories";

    public static URL buildSearchCategories() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATEGORIE);
        URL url = new URL(builder.build().toString());
        return url;
    }
}


