package com.example.projetmobile_lucasdamiens.webservice;


import android.util.JsonReader;
import android.util.Log;
import com.example.projetmobile_lucasdamiens.Objet;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class JSONResponseHandlerCatalog {

    private ArrayList<Objet> catalog;

    public int index;

    public JSONResponseHandlerCatalog(ArrayList<Objet> catalog) { this.catalog=catalog; }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCatalog(reader);
        } finally {
            reader.close();
        }
    }

    public void readCatalog(JsonReader reader) throws IOException {
        reader.beginObject();
        index=0;
        while (reader.hasNext()) {
            String name = reader.nextName();
            catalog.add(new Objet(name));
            readObjet(reader);
            index=index+1;
        }
        reader.endObject();
    }

    public void readObjet(JsonReader reader) throws IOException {
        reader.beginObject();
        while(reader.hasNext()) {
            String name = reader.nextName();
            if(name.equals("name")) {
                catalog.get(index).setName(reader.nextString());
            } else if (name.equals("categories")){
                reader.beginArray();
                while(reader.hasNext())
                {
                    catalog.get(index).getCategories().add(reader.nextString());
                }
                reader.endArray();
            } else if(name.equals("brand")) {
                catalog.get(index).setBrand(reader.nextString());
            } else if(name.equals("description")) {
                catalog.get(index).setDescription(reader.nextString());
            } else if(name.equals("timeframe")) {
                reader.beginArray();
                while(reader.hasNext())
                {
                    catalog.get(index).getTimeFrame().add(reader.nextInt());
                }
                reader.endArray();
            } else if(name.equals("year")) {
                catalog.get(index).setYear(reader.nextInt());
            } else if(name.equals("technicalDetails")) {
                reader.beginArray();
                while(reader.hasNext())
                {
                    catalog.get(index).getTechnicalDetails().add(reader.nextString());
                }
                reader.endArray();
            } else if(name.equals("working")) {
                catalog.get(index).setWorking(reader.nextBoolean());
            } else if(name.equals("pictures")) {
                reader.beginObject();
                while(reader.hasNext()) {
                    catalog.get(index).getPictures().add(reader.nextName().toString());
                    catalog.get(index).getPicturesDescription().add(reader.nextString());
                }
                reader.endObject();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }
}
