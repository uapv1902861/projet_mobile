package com.example.projetmobile_lucasdamiens.webservice;

import android.util.JsonReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class JSONResponseHandlerIds {

    private ArrayList<String> ObjetIds;

    public JSONResponseHandlerIds(ArrayList<String> ids) { this.ObjetIds=ids; }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try{
            readIds(reader);
        } finally {
            reader.close();
        }
    }

    public void readIds(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext())
        {
            String name = reader.nextString();
            ObjetIds.add(name);
        }
        reader.endArray();
    }
}
