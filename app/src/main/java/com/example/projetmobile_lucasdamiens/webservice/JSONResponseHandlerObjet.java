package com.example.projetmobile_lucasdamiens.webservice;

import android.util.JsonReader;

import com.example.projetmobile_lucasdamiens.Objet;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerObjet {

    private Objet objet;

    public JSONResponseHandlerObjet(Objet item) { this.objet=item; }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readObjet(reader);
        } finally {
            reader.close();
        }
    }

    public void readObjet(JsonReader reader) throws IOException {
        reader.beginObject();
        while(reader.hasNext()) {
            String name = reader.nextName();
            if(name.equals("name")) {
                objet.setName(reader.nextString());
            } else if (name.equals("categories")){
                reader.beginArray();
                while(reader.hasNext())
                {
                    objet.getCategories().add(reader.nextString());
                }
                reader.endArray();
            } else if(name.equals("brand")) {
                objet.setBrand(reader.nextString());
            } else if(name.equals("description")) {
                objet.setDescription(reader.nextString());
            } else if(name.equals("timeframe")) {
                reader.beginArray();
                while(reader.hasNext())
                {
                    objet.getTimeFrame().add(reader.nextInt());
                }
                reader.endArray();
            } else if(name.equals("year")) {
                objet.setYear(reader.nextInt());
            } else if(name.equals("technicalDetails")) {
                reader.beginArray();
                while(reader.hasNext())
                {
                    objet.getTechnicalDetails().add(reader.nextString());
                }
                reader.endArray();
            } else if(name.equals("working")) {
                objet.setWorking(reader.nextBoolean());
            } else if(name.equals("pictures")) {
                reader.beginObject();
                while(reader.hasNext()) {
                    objet.getPictures().add(reader.nextName().toString());
                    objet.getPicturesDescription().add(reader.nextString());
                }
                reader.endObject();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }
}
