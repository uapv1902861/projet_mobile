package com.example.projetmobile_lucasdamiens;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.projetmobile_lucasdamiens.webservice.JSONResponseHandlerCatalog;
import com.example.projetmobile_lucasdamiens.webservice.JSONResponseHandlerCategories;
import com.example.projetmobile_lucasdamiens.webservice.JSONResponseHandlerIds;
import com.example.projetmobile_lucasdamiens.webservice.WebServiceUrl;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView catalog;
    CustomListViewAdapterObjet adapter;
    ArrayList<String> cat;
    ArrayList<Objet> webCatalog;
    ProgressDialog progressDialog;
    Intent myIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyAsyncTasks asyncTask = new MyAsyncTasks();
                asyncTask.execute();
            }
        });

        catalog = (ListView) findViewById(R.id.catalog);

        catalog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String objetId = webCatalog.get(position).getId();
                myIntent = new Intent(MainActivity.this, ObjetActivity.class).putExtra("Id", objetId);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_tri) {
            ArrayList<Objet> triObjet = new ArrayList<Objet>();

            triObjet = triAlpha(webCatalog);

            webCatalog=triObjet;

            adapter = new CustomListViewAdapterObjet(this, webCatalog);

            catalog.setAdapter(adapter);
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateCatalog(){

        adapter = new CustomListViewAdapterObjet(this, webCatalog);

        catalog.setAdapter(adapter);
    }

    public class MyAsyncTasks extends AsyncTask<String, String, ArrayList<Objet>> {

        private String thumbnail="";
        private String picture="";

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Mise à jours en cours...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected ArrayList<Objet> doInBackground(String... params)
        {
            ArrayList<Objet> items = new ArrayList<Objet>();

            try
            {
                URL url;
                JSONResponseHandlerCatalog JSONCatalog = new JSONResponseHandlerCatalog(items);
                HttpURLConnection urlConnection = null;

                try
                {
                    url = WebServiceUrl.buildSearchCatalog();
                    urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream in = urlConnection.getInputStream();

                    JSONCatalog.readJsonStream(in);

                }
                catch (MalformedURLException e)
                {
                    e.printStackTrace();
                }

                try {
                    for(int i = 0 ; i<items.size() ; i++) {
                        url = WebServiceUrl.buildSearchThumbnail(items.get(i).getId());

                        InputStream in = (InputStream) new URL(url.toString()).getContent();

                        Drawable d = Drawable.createFromStream(in, thumbnail);

                        items.get(i).setThumbnail(d);

                    }
                }
                catch (MalformedURLException e)
                {
                    e.printStackTrace();
                }

                try
                {
                    url = WebServiceUrl.buildSearchCategories();
                    urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream in = urlConnection.getInputStream();

                    cat= new ArrayList<String>();

                    JSONResponseHandlerCategories JSONCategories = new JSONResponseHandlerCategories(cat);

                    JSONCategories.readJsonStream(in);

                }
                catch (MalformedURLException e)
                {
                    e.printStackTrace();
                }

            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return items;
        }

        @Override
        protected void onPostExecute(ArrayList<Objet> items)
        {
            progressDialog.dismiss();
            webCatalog = items;

            updateCatalog();
        }

    }

    public ArrayList<Objet> triAlpha(ArrayList<Objet> catalog) {
        ArrayList<Objet> tri = new ArrayList<Objet>();
        ArrayList<String> string = new ArrayList<String>();

        for(int i = 0; i<catalog.size(); i++)
        {
            string.add(catalog.get(i).getName());
        }

        Collections.sort(string);

        for(int i = 0; i<catalog.size(); i++)
        {
            for(int j=0; j<catalog.size(); j++)
            {
                 if(catalog.get(j).getName().equals(string.get(i)))
                     tri.add(catalog.get(j));
            }
        }

        return tri;
    }

    public ArrayList<Objet> triCategories(String categorie) {
        ArrayList<Objet> tri = new ArrayList<Objet>();

        for(int i = 0; i<webCatalog.size(); i++)
        {
            for(int j=0; j<webCatalog.get(i).getCategories().size(); j++)
            {
                if(webCatalog.get(j).getCategories().get(j).equals(categorie))
                    tri.add(webCatalog.get(j));
            }
        }
        return tri;
    }

}
