package com.example.projetmobile_lucasdamiens;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;

public class Objet {

    private String id;

    private String name;
    private ArrayList<String> categories;
    private String description;
    private int year;
    private String brand;
    private ArrayList<String> technicalDetails;
    private ArrayList<Integer> timeFrame;
    private boolean working;
    private Drawable thumbnail;
    private ArrayList<String> pictures;
    private ArrayList<String> picturesDescription;
    private ArrayList<Drawable> images;

    public Objet(String id){
        setId(id);
        setBrand("No brand");
        this.categories= new ArrayList<String>();
        this.technicalDetails= new ArrayList<String>();
        this.timeFrame= new ArrayList<Integer>();
        this.pictures= new ArrayList<String>();
        this.picturesDescription= new ArrayList<String>();
        this.images= new ArrayList<Drawable>();
        this.working=false;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id=id; }

    public String getName() { return name; }

    public void setName(String name) { this.name=name; }

    public ArrayList<String> getCategories() { return categories; }

    public void  setCategories(ArrayList<String> categories) { this.categories=categories;}

    public void setDescription(String description) { this.description = description; }

    public String getDescription() { return description; }

    public int getYear() { return year; }

    public void setYear(int year) { this.year = year; }

    public String getBrand() { return brand; }

    public void setBrand(String brand) { this.brand=brand; }

    public ArrayList<String> getTechnicalDetails() { return technicalDetails;}

    public void setTechnicalDetails(ArrayList<String> technicalDetails) { this.technicalDetails=technicalDetails;}

    public ArrayList<Integer> getTimeFrame() { return timeFrame;}

    public void setTimeFrame(ArrayList<Integer> timeFrame) { this.timeFrame=timeFrame;}

    public boolean isWorking() {return working;}

    public void setWorking(boolean working) { this.working=working; }

    public ArrayList<String> getPictures() { return pictures; }

    public void setPictures(ArrayList<String> pictures) { this.pictures=pictures;}

    public ArrayList<String> getPicturesDescription() { return picturesDescription; }

    public void setPicturesDescription(ArrayList<String> picturesDescription) { this.picturesDescription=picturesDescription;}

    public void setACategorie(String categorie, int index) { categories.set(index, categorie); }

    public void setADetail(String detail, int index) { technicalDetails.set(index, detail); }

    public void setATimeFrame(int timeframe, int index) { timeFrame.set(index, timeframe); }

    public void setThumbnail(Drawable thumbnail) { this.thumbnail = thumbnail; }

    public Drawable getThumbnail(){ return this.thumbnail; }

    public ArrayList<Drawable> getImages() { return images; }

    public void setImages(ArrayList<Drawable> images) { this.images = images; }

    public String getAllCategories(){
        String cat="";

        for(int i = 0 ; i<categories.size(); i++)
        {
            if(i !=0)
                cat= cat + ", " + categories.get(i);
            else
                cat=categories.get(i);
        }

        return cat;
    }

    public String getAllDetails(){
        String details="";

        for(int i = 0 ; i<technicalDetails.size(); i++)
        {
            if(i !=0)
                details= details + ", " + technicalDetails.get(i);
            else
                details=technicalDetails.get(i);
        }

        return details;
    }

    public String getYearAndTimeFrame(){
        String ytf="";

        ytf = Integer.toString(year);

        for(int i = 0 ; i<timeFrame.size(); i++)
        {
            if(i !=0)
                ytf= ytf + ", " + timeFrame.get(i)+"'s";
            else
                ytf="/"+timeFrame.get(i)+"'s";
        }

        return ytf;
    }

    public String getWorking(){
        String etat="";

        if(this.working==true)
            etat="Fonctionnel";
        else
            etat="Défectueux";

        return etat;
    }

}
