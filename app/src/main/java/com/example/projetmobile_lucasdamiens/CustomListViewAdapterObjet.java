package com.example.projetmobile_lucasdamiens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class CustomListViewAdapterObjet extends BaseAdapter {

    private ArrayList<Objet> catalog;
    private LayoutInflater layoutInflater;
    private Context context;

    public CustomListViewAdapterObjet(Context context, ArrayList<Objet> catalog){
        this.catalog=catalog;
        layoutInflater = LayoutInflater.from(context);
        this.context=context;
    }

    @Override
    public int getCount() {
        return catalog.size();
    }

    @Override
    public Object getItem(int position) {
        return catalog.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null) {
            convertView = layoutInflater.inflate(R.layout.objet, null);
            holder = new ViewHolder();

            holder.name = (TextView) convertView.findViewById(R.id.lignea);
            holder.categories = (TextView) convertView.findViewById(R.id.ligneb);
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.imageView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(catalog.get(position).getName());
        holder.categories.setText(catalog.get(position).getAllCategories());
        holder.thumbnail.setImageDrawable(catalog.get(position).getThumbnail());

        return convertView;
    }

    static class ViewHolder {
        TextView name;
        TextView categories;
        ImageView thumbnail;
    }
}
